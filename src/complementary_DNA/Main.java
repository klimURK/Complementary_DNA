package complementary_DNA;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		String dna;
		Scanner in = new Scanner(System.in);
		dna= in.next();
		System.out.println(makeComplement(dna));
		in.next();
		in.close();
	}

	public static String makeComplement(String dna) {
		String newDNA = new String();
		for (int i = 0; i < dna.length(); i++) {
			switch (dna.charAt(i)) {
			case 'A':
				newDNA = newDNA + 'T';
				break;
			case 'T':
				newDNA = newDNA + 'A';
				break;
			case 'C':
				newDNA = newDNA + 'G';
				break;
			case 'G':
				newDNA = newDNA + 'C';
				break;
			default:
				break;
			}
		}
		return newDNA;
	}
}
